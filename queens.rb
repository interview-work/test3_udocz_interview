class Game

  attr_reader :queen, :board, :matrix_sols
  
  def initialize(queen)
    @queen = queen
    @matrix_sols = []
  end

  def get_queen(board=Array.new(@queen){Array.new(@queen, "0")}, col=0, step = 0)
    if col == 0
      board[step][0] = "1"
      col = col + 1
    end
    if col >= @queen
        @matrix_sols << board
      return true    
    end
    (0..@queen-1).each do |i|
      if is_valid?(board, i, col)
        board[i][col] = "1"
        return true if get_queen(board, col+1)
      end
      board[i][col] = "0"
    end
    return false
  end

  def is_valid?(board, pos_i, pos_j)
    (0..pos_j-1).each do |k|
      if board[pos_i][k] == "1"
        return false
      end
      if pos_i+k+1 < board.size and pos_j-k-1 >= 0
        return false if board[pos_i+k+1][pos_j-k-1] == "1"
      end
      if  pos_i-k-1 >= 0 and pos_j-k-1 >= 0 
        return false if board[pos_i-k-1][pos_j-k-1] == "1"
      end 
    end
    true
  end

  def get_all_solutions
    (0..@queen-1).each do |i|
      get_queen(Array.new(@queen){Array.new(@queen, "0")}, 0, i)
    end 
  end
end
puts "Digita numero para tamaño de tablero"
n = gets.chomp
game = Game.new(n.to_i)
game.get_all_solutions
game.matrix_sols.each_with_index do |solution, index|
  puts "Imprimiendo solucion #{index}"
  pp solution
end